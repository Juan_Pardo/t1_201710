package model.logic;

import java.util.Iterator;

import model.data_structures.IntegerBag;

public class BigBagOperations  <T>

{
	public double computeMean(IntegerBag<Integer> bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegerBag<Integer> bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	
	//Operacion #1
	public  int  getMin(IntegerBag<Integer> bag)
	{
		int min= Integer.MAX_VALUE;
		int value;
		if (bag!=null)
		{
			Iterator<Integer> iter=  bag.getIterator();
			while(iter.hasNext())
			{
				value=iter.next();
				if (min>value) {
					min=value;
				}
			}
		}
		return min;
	}
	
	
	//Operacion #2
	public double getMidValue(IntegerBag<Integer> bag)
	{
		double result=0;
		int min=getMin(bag);
		int max=getMax(bag);
		result= (min+max)/2;
		return result;
	}
	
	//Operacion #3
	public int getTotalValue(IntegerBag<Integer> bag)
	{
		int resp=0;
		if (bag!=null)
		{
			Iterator<Integer> iter=  bag.getIterator();
			while(iter.hasNext())
			{
				resp+=iter.next();
				
				
			}
		}
		return resp;
	}
}
