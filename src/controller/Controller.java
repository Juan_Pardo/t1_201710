package controller;

import java.util.ArrayList;

import model.data_structures.IntegerBag;
import model.logic.BigBagOperations;

public class Controller {

	private static BigBagOperations model = new BigBagOperations();
	
	
	public static IntegerBag createBag(ArrayList<Integer> values){
         return new IntegerBag(values);		
	}
	
	
	public static double getMean(IntegerBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegerBag bag){
		return model.getMax(bag);
	}
	
	public static double getMin(IntegerBag bag)
	{
		return model.getMin(bag);
	}
	
	public static double getMid(IntegerBag bag)
	{
		return model.getMidValue(bag);
	}
	
	public static double getTotal(IntegerBag bag)
	{
		return model.getTotalValue(bag);
	}
	
}
